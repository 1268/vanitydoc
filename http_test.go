// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package main

import (
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

func TestHTTP(t *testing.T) {
	s := server{
		fsys:       os.DirFS("."),
		modulePath: "code.pfad.fr/vanitydoc",
	}
	get := func(target string) *httptest.ResponseRecorder {
		rw := httptest.NewRecorder()
		r := httptest.NewRequest("GET", target, nil)
		s.ServeHTTP(rw, r)
		return rw
	}

	t.Run("redirect root", func(t *testing.T) {
		rw := get("/")
		{
			got, want := rw.Result().StatusCode, 302
			if got != want {
				t.Errorf("GET / got %d, expected %d", got, want)
			}
		}
		{
			got, want := rw.Result().Header.Get("Location"), "/vanitydoc"
			if got != want {
				t.Errorf("GET / got Location: %q, expected %q", got, want)
			}
		}
	})
	t.Run("get vanitydoc", func(t *testing.T) {
		rw := get("/vanitydoc")
		{
			got, want := rw.Result().StatusCode, 200
			if got != want {
				t.Errorf("GET / got %d, expected %d", got, want)
			}
		}
		if !strings.Contains(rw.Body.String(), "code.pfad.fr/vanitydoc") {
			t.Errorf("could not find %q within response", "code.pfad.fr/vanitydoc")
		}
		if strings.Contains(rw.Body.String(), "LICENSES") {
			t.Errorf("should not find %q within response", "LICENSES")
		}
	})
	t.Run("get fallback", func(t *testing.T) {
		rw := get("/vanitydoc?fallback")
		{
			got, want := rw.Result().StatusCode, 200
			if got != want {
				t.Errorf("GET / got %d, expected %d", got, want)
			}
		}
		if !strings.Contains(rw.Body.String(), "code.pfad.fr/vanitydoc") {
			t.Errorf("could not find %q within response", "code.pfad.fr/vanitydoc")
		}
		if !strings.Contains(rw.Body.String(), "LICENSES") {
			t.Errorf("could not find %q within fallback", "LICENSES")
		}
	})
	t.Run("get vanitydoc.css", func(t *testing.T) {
		rw := get("/vanitydoc.css")
		{
			got, want := rw.Result().StatusCode, 200
			if got != want {
				t.Errorf("GET / got %d, expected %d", got, want)
			}
		}
	})
	t.Run("get 404", func(t *testing.T) {
		rw := get("/vanitydoc/404")
		{
			got, want := rw.Result().StatusCode, 404
			if got != want {
				t.Errorf("GET / got %d, expected %d", got, want)
			}
		}
	})
}

func TestGoModCache(t *testing.T) {
	s := server{
		fsys:       os.DirFS("."),
		modulePath: "code.pfad.fr/vanitydoc",
		gomodcache: os.DirFS("example/testdata/gomod"),
	}
	get := func(target string) *httptest.ResponseRecorder {
		rw := httptest.NewRecorder()
		r := httptest.NewRequest("GET", target, nil)
		s.ServeHTTP(rw, r)
		return rw
	}

	t.Run("get vanitydoc", func(t *testing.T) {
		rw := get("/vanitydoc")
		{
			got, want := rw.Result().StatusCode, 200
			if got != want {
				t.Errorf("GET / got %d, expected %d", got, want)
			}
		}
		if !strings.Contains(rw.Body.String(), "code.pfad.fr/vanitydoc") {
			t.Errorf("could not find %q within response", "code.pfad.fr/vanitydoc")
		}
		if strings.Contains(rw.Body.String(), "LICENSES") {
			t.Errorf("should not find %q within response", "LICENSES")
		}
	})
	t.Run("gomodcache", func(t *testing.T) {
		t.Run("devf", func(t *testing.T) {
			rw := get("/code.pfad.fr/devf")
			{
				got, want := rw.Result().StatusCode, 200
				if got != want {
					t.Errorf("GET / got %d, expected %d", got, want)
				}
			}
			if !strings.Contains(rw.Body.String(), "devf") {
				t.Errorf("could not find %q within response", "devf")
			}
			if !strings.Contains(rw.Body.String(), "Forge") {
				t.Errorf("could not find %q within response", "Forge")
			}
		})
		t.Run("devf/livereload", func(t *testing.T) {
			rw := get("/code.pfad.fr/devf/livereload")
			{
				got, want := rw.Result().StatusCode, 200
				if got != want {
					t.Errorf("GET / got %d, expected %d", got, want)
				}
			}
			if !strings.Contains(rw.Body.String(), "SingleLineRequestLogger") {
				t.Errorf("could not find %q within response", "SingleLineRequestLogger")
			}

			if !strings.Contains(rw.Body.String(), `<a href="/code.pfad.fr/devf/">code.pfad.fr/devf</a>`) {
				t.Errorf("could not find %q within response", "Link to code.pfad.fr/devf")
				t.Log(rw.Body.String())
			}
		})
		t.Run("devf@v0.4.0", func(t *testing.T) {
			rw := get("/code.pfad.fr/devf@v0.4.0")
			{
				got, want := rw.Result().StatusCode, 200
				if got != want {
					t.Errorf("GET / got %d %s, expected %d", got, rw.Result().Header.Get("Location"), want)
				}
			}
			if !strings.Contains(rw.Body.String(), "devf") {
				t.Errorf("could not find %q within response", "devf")
			}
			if !strings.Contains(rw.Body.String(), "Forge") {
				t.Errorf("could not find %q within response", "Forge")
			}
		})
		t.Run("devf@v0.4.0/livereload", func(t *testing.T) {
			rw := get("/code.pfad.fr/devf@v0.4.0/livereload")
			{
				got, want := rw.Result().StatusCode, 200
				if got != want {
					t.Errorf("GET / got %d, expected %d", got, want)
				}
			}
			if !strings.Contains(rw.Body.String(), "SingleLineRequestLogger") {
				t.Errorf("could not find %q within response", "SingleLineRequestLogger")
			}

			if !strings.Contains(rw.Body.String(), `<a href="/code.pfad.fr/devf/">code.pfad.fr/devf</a>`) {
				t.Errorf("could not find %q within response", "Link to code.pfad.fr/devf")
				t.Log(rw.Body.String())
			}
		})
		t.Run("devf/404", func(t *testing.T) {
			rw := get("/code.pfad.fr/devf/404")
			{
				got, want := rw.Result().StatusCode, 404
				if got != want {
					t.Errorf("GET / got %d, expected %d", got, want)
				}
			}
		})
		t.Run("non-existing-domain", func(t *testing.T) {
			rw := get("/not-found.pfad.fr/devf")
			{
				got, want := rw.Result().StatusCode, 404
				if got != want {
					t.Errorf("GET / got %d, expected %d", got, want)
				}
			}
		})
		t.Run("non-existing-version", func(t *testing.T) {
			rw := get("/code.pfad.fr/devf@v0.42.0")
			{
				got, want := rw.Result().StatusCode, 404
				if got != want {
					t.Errorf("GET / got %d, expected %d", got, want)
				}
			}
		})
	})
	t.Run("redirect root", func(t *testing.T) {
		rw := get("/")
		{
			got, want := rw.Result().StatusCode, 302
			if got != want {
				t.Errorf("GET / got %d, expected %d", got, want)
			}
		}
		{
			got, want := rw.Result().Header.Get("Location"), "/vanitydoc"
			if got != want {
				t.Errorf("GET / got Location: %q, expected %q", got, want)
			}
		}
	})
	t.Run("get fallback", func(t *testing.T) {
		rw := get("/vanitydoc?fallback")
		{
			got, want := rw.Result().StatusCode, 200
			if got != want {
				t.Errorf("GET / got %d, expected %d", got, want)
			}
		}
		if !strings.Contains(rw.Body.String(), "code.pfad.fr/vanitydoc") {
			t.Errorf("could not find %q within response", "code.pfad.fr/vanitydoc")
		}
		if !strings.Contains(rw.Body.String(), "LICENSES") {
			t.Errorf("could not find %q within fallback", "LICENSES")
		}
	})
	t.Run("get vanitydoc.css", func(t *testing.T) {
		rw := get("/vanitydoc.css")
		{
			got, want := rw.Result().StatusCode, 200
			if got != want {
				t.Errorf("GET / got %d, expected %d", got, want)
			}
		}
	})
	t.Run("get 404", func(t *testing.T) {
		rw := get("/vanitydoc/404")
		{
			got, want := rw.Result().StatusCode, 404
			if got != want {
				t.Errorf("GET / got %d, expected %d", got, want)
			}
		}
	})
}

func TestEscapeModulePath(t *testing.T) {
	for input, expected := range map[string]string{
		"code.pfad.fr/vanitydoc": "code.pfad.fr/vanitydoc",
		"!invaliD":               "!invaliD",
		"code.pfad.fr/Pascal":    "code.pfad.fr/!pascal",
	} {
		t.Run(input, func(t *testing.T) {
			got := escapeModulePath(input)
			if got != expected {
				t.Errorf("expected %q, got: %q", expected, got)
			}
		})
	}
}
