// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
// SPDX-FileCopyrightText: 2013 The Go Authors
//
// SPDX-License-Identifier: BSD-3-Clause

package template

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/doc"
	"go/doc/comment"
	"go/token"
	"html"
	"html/template"
	"io"
	"io/fs"
	"mime"
	"path"
	"runtime/debug"
	"strconv"
	"strings"
	"text/tabwriter"
)

var (
	stylesheetTemplate = template.Must(template.New("name").Parse(`<link href="{{.}}" rel="stylesheet">` + "\n"))
	linkTemplate       = template.Must(template.New("name").Parse(`<a href="{{.Href}}" rel="{{.Rel}}" title="View Source">{{.Text}}</a>` + "\n"))
	spanTemplate       = template.Must(template.New("name").Parse(`<span id="{{.}}">{{.}}</span>`))
	faviconTemplate    = template.Must(template.New("name").Parse(`<link rel="icon" {{with .Type}}type="{{.}}" {{end}}href="{{.Href}}" />`))
)

func executeTemplate(t *template.Template, data any) (string, error) {
	buf := &bytes.Buffer{}
	err := t.Execute(buf, data)
	return buf.String(), err
}

// HTMLRenderer provides the generation of HTML documentation.
type HTMLRenderer struct {
	Favicon     string
	Stylesheets []string
	// AdditionalFS and AdditionalPattern can be optionally provided
	// to override some components
	AdditionalFS       fs.FS
	AdditionalPatterns []string

	BaseURL string
	DocSite string // defaults to "https://pkg.go.dev"
}

// Execute applies a parsed template to the template data, and writes the output to w.
func (hr HTMLRenderer) Execute(out io.Writer, td TemplateData) error {
	domain, baseURL, found := strings.Cut(td.ModulePath, "/")
	if found {
		baseURL = "/" + baseURL
	}
	if hr.BaseURL != "" {
		baseURL = hr.BaseURL
	}
	commentPrinter := td.Package.Printer()
	docSite := hr.DocSite
	if docSite == "" {
		docSite = "https://pkg.go.dev"
	}
	commentPrinter.DocLinkURL = docLinkUrl(docSite, domain, func(root string, dl *comment.DocLink) string {
		switch {
		case dl.Name == "":
			return root
		case dl.Recv != "":
			return root + "#" + dl.Recv + "." + dl.Name
		default:
			return root + "#" + dl.Name
		}
	})
	readmePrinter := *commentPrinter
	readmePrinter.HeadingLevel = 2
	goPrinter := newGoPrinter(td.FileSet)
	astFormatter := astFormatter{
		Fallback:   goPrinter.PrettyPrintAST,
		DocLinkURL: commentPrinter.DocLinkURL,
	}
	pkgFolder := strings.Join(td.PackageChain, "/")
	t, err := template.New("ROOT").Funcs(template.FuncMap{
		"metaTags": func() (template.HTML, error) {
			buf := &bytes.Buffer{}
			err := td.VCS.MetaTags(buf)
			if err != nil {
				return "", err
			}
			err = td.VCS.GoImport(buf, td.ModulePath)
			if err != nil {
				return "", err
			}
			err = td.VCS.GoSource(buf, td.ModulePath)
			if err != nil {
				return "", err
			}

			for _, s := range hr.Stylesheets {
				err = stylesheetTemplate.Execute(buf, s)
				if err != nil {
					return "", err
				}
			}

			if hr.Favicon != "" {
				err = faviconTemplate.Execute(buf, struct{ Type, Href string }{
					Type: mime.TypeByExtension(path.Ext(hr.Favicon)),
					Href: hr.Favicon,
				})
				if err != nil {
					return "", err
				}
			}
			return template.HTML(buf.String()), nil
		},
		"vanitydocVersion": func() string {
			bi, ok := debug.ReadBuildInfo()
			if !ok {
				return ""
			}
			if bi.Main.Path == "code.pfad.fr/vanitydoc" {
				return bi.Main.Version
			}
			for _, d := range bi.Deps {
				if d.Path != "code.pfad.fr/vanitydoc" {
					continue
				}
				return d.Version
			}
			return ""
		},
		"readme": func(s string) template.HTML {
			return template.HTML(readmePrinter.HTML(td.Package.Parser().Parse(s)))
		},
		"comment": func(s string) template.HTML {
			return template.HTML(commentPrinter.HTML(td.Package.Parser().Parse(s)))
		},
		"dd": func(v any) (string, error) {
			buf := &bytes.Buffer{}
			err := ast.Fprint(buf, td.FileSet, v, ast.NotNilFilter)
			return buf.String(), err
		},
		"formatValueSpecs": func(decl *ast.GenDecl) (template.HTML, error) {
			// var or const declation

			// align assignments
			var out bytes.Buffer
			tw := tabwriter.NewWriter(&out, 1, 4, 1, ' ', tabwriter.FilterHTML|tabwriter.StripEscape|tabwriter.TabIndent)

			var multilineDecl *bool
			var previous token.Pos
			for _, s := range decl.Specs {
				if previous.IsValid() && s.Pos() > previous+2 {
					// add "cosmetic" vertical spacing
					tw.Write([]byte{'\n'})
				}
				previous = s.End()

				switch v := s.(type) {
				case *ast.ValueSpec:
					if multilineDecl == nil {
						multilineDecl = new(bool)
						tw.Write([]byte(v.Names[0].Obj.Kind.String() + " "))
						if len(decl.Specs) > 1 || v.Doc != nil {
							*multilineDecl = true
							tw.Write([]byte("(\n"))
						}
					}

					if v.Doc != nil {
						// top comment
						for _, c := range v.Doc.List {
							tw.Write([]byte{'\t', tabwriter.Escape})
							tw.Write([]byte(`<span class="comment">` + html.EscapeString(c.Text) + "</span>"))
							tw.Write([]byte{tabwriter.Escape, '\n'})
						}
					}
					if *multilineDecl {
						tw.Write([]byte{'\t'})
					}

					// print names
					for i, name := range v.Names {
						if i > 0 {
							tw.Write([]byte(", "))
						}
						if err := spanTemplate.Execute(tw, name.Name); err != nil {
							return "", err
						}
					}

					// print type
					if v.Type != nil {
						tw.Write([]byte{'\t'})
						if err := astFormatter.formatExpr(tw, v.Type, 0, true); err != nil {
							return "", err
						}
					}

					// print values
					for i, value := range v.Values {
						if i == 0 {
							tw.Write([]byte("\t= "))
						} else {
							tw.Write([]byte(", "))
						}

						// intermediary buffer to prevent tabwriter conflicts
						var buf bytes.Buffer
						if err := astFormatter.formatExpr(&buf, value, 0, true); err != nil {
							return "", err
						}
						tw.Write([]byte{tabwriter.Escape})
						buf.WriteTo(tw)
						tw.Write([]byte{tabwriter.Escape})
					}

					if v.Comment != nil {
						// trailing comment
						tw.Write([]byte{'\t', tabwriter.Escape})
						tw.Write([]byte(`<span class="comment">// ` + html.EscapeString(strings.TrimSpace(v.Comment.Text())) + `</span>`))
						tw.Write([]byte{tabwriter.Escape, '\n'})
						previous = v.Comment.End()
					} else {
						tw.Write([]byte{'\n'})
					}
				default:
					return "", fmt.Errorf("unsupported type inside formatValueSpecs: %T", v)
				}
			}
			if multilineDecl != nil && *multilineDecl {
				tw.Write([]byte{')'})
			}

			if err := tw.Flush(); err != nil {
				return "", err
			}

			return template.HTML(out.String()), nil
		},
		"formatGenDecl": func(decl *ast.GenDecl) (template.HTML, error) {
			// type declation
			var buf bytes.Buffer
			err := astFormatter.formatGenDecl(&buf, decl)
			return template.HTML(buf.String()), err
		},
		"formatFuncDeclWithoutLink": func(decl *ast.FuncDecl) (template.HTML, error) {
			// func declation without doc link (since already in a link)
			var buf bytes.Buffer
			err := astFormatter.formatFuncDecl(&buf, decl, false)
			return template.HTML(buf.String()), err
		},
		"formatFuncDecl": func(decl *ast.FuncDecl) (template.HTML, error) {
			// func declation
			var buf bytes.Buffer
			err := astFormatter.formatFuncDecl(&buf, decl, true)
			return template.HTML(buf.String()), err
		},
		"formatExampleStandalone": goPrinter.PrettyPrintAST,
		"formatExampleTest":       goPrinter.ExampleTest,
		"lineForgeLink": func(pos token.Pos, text string, textOnlyOK bool) (template.HTML, error) {
			position := td.FileSet.Position(pos)
			href := td.VCS.Forge.LineURL(path.Join(pkgFolder, position.Filename), td.Ref, strconv.Itoa(position.Line))
			if position.Line == 0 || href == "" {
				if textOnlyOK {
					return template.HTML(text), nil
				}
				return "", nil
			}

			txt, err := executeTemplate(linkTemplate, struct {
				Href, Rel, Text string
			}{
				Href: href,
				Rel:  "noopener nofollow",
				Text: text,
			})
			return template.HTML(txt), err
		},
		"fileForgeLink": func(name string) (template.HTML, error) {
			href := td.VCS.Forge.FileURL(path.Join(pkgFolder, name), td.Ref)
			if href == "" {
				return template.HTML(template.HTMLEscapeString(name)) + "\n", nil
			}
			txt, err := executeTemplate(linkTemplate, struct {
				Href, Rel, Text string
			}{
				Href: href,
				Rel:  "noopener nofollow",
				Text: name,
			})
			return template.HTML(txt), err
		},
		/* "dirForgeLink": func(text string) (template.HTML, error) {
			href := td.VCS.Forge.DirURL(pkgFolder, td.Ref)
			if href == "" {
				return template.HTML(template.HTMLEscapeString(text)), nil
			}
			txt, err := executeTemplate(linkTemplate, struct {
				Href, Rel, Text string
			}{
				Href: href,
				Rel:  "noopener nofollow",
				Text: text,
			})
			return template.HTML(txt), err
		}, */
		"hasExamples": hasExamples,
		"breadcrumbs": func() []link {
			return breadcrumbs(td.ModulePath, baseURL, td.PackageChain, false)
		},
		"sections": func() []link {
			var sections []link

			if len(td.Package.Consts) > 0 || len(td.Package.Vars) > 0 || len(td.Package.Funcs) > 0 || len(td.Package.Types) > 0 {
				sections = append(sections, link{
					Text: "Index",
					Href: "#pkg-index",
				})
			}

			if hasExamples(td.Package) {
				sections = append(sections, link{
					Text: "Examples",
					Href: "#pkg-examples",
				})
			}

			if len(td.Package.Notes["BUG"]) > 0 {
				sections = append(sections, link{
					Text: "Bugs",
					Href: "#pkg-note-bug",
				})
			}

			sections = append(sections, link{
				Text: "Files",
				Href: "#pkg-files",
			})

			if len(td.Directories) > 0 {
				sections = append(sections, link{
					Text: "Directories",
					Href: "#pkg-subdirectories",
				})
			}

			if (td.VCS.Forge.Summary != "" || len(td.VCS.Clone) > 0) && td.ModulePath == td.Package.ImportPath {
				sections = append(sections, link{
					Text: "Forge",
					Href: "#pkg-forge",
				})
			}

			return sections
		},
		"currentcrumb": func() string {
			l := len(td.PackageChain)
			if l == 0 {
				return td.ModulePath
			}
			return td.PackageChain[l-1]
		},
		"subPackageURL": func(name string) string {
			return baseURL + "/" + strings.Join(append(td.PackageChain, name), "/")
		},
		"differentPackageName": func() bool {
			return td.Package.Name != "" && !strings.HasSuffix(td.Package.ImportPath, "/"+td.Package.Name)
		},
	}).ParseFS(templatesFS, "templates/index.gohtml", "templates/layout.gohtml")
	if err != nil {
		return err
	}
	if hr.AdditionalFS != nil && len(hr.AdditionalPatterns) > 0 {
		t, err = t.ParseFS(hr.AdditionalFS, hr.AdditionalPatterns...)
		if err != nil {
			return err
		}
	}
	return t.Execute(out, td)
}

func hasExamples(d *doc.Package) bool {
	if len(d.Examples) > 0 {
		return true
	}
	for _, f := range d.Funcs {
		if len(f.Examples) > 0 {
			return true
		}
	}
	for _, t := range d.Types {
		if len(t.Examples) > 0 {
			return true
		}
		for _, f := range t.Funcs {
			if len(f.Examples) > 0 {
				return true
			}
		}
		for _, m := range t.Methods {
			if len(m.Examples) > 0 {
				return true
			}
		}
	}
	return false
}

// Execute applies a parsed template to the template data, and writes the output to w.
func (hr HTMLRenderer) NotFound(out io.Writer, baseURL, modulePath, importPath, ref string) error {
	after, _ := strings.CutPrefix(importPath, modulePath)
	packageChain := strings.Split(strings.Trim(after, "/"), "/")
	bbs := breadcrumbs(modulePath, baseURL, packageChain, false)
	moduleHref := "/" + modulePath
	if len(bbs) > 0 {
		moduleHref = bbs[0].Href
	}

	t, err := template.New("ROOT").Funcs(template.FuncMap{
		"metaTags": func() (template.HTML, error) {
			buf := &bytes.Buffer{}
			for _, s := range hr.Stylesheets {
				err := stylesheetTemplate.Execute(buf, s)
				if err != nil {
					return "", err
				}
			}
			return template.HTML(buf.String()), nil
		},
		"breadcrumbs": func() []link {
			return breadcrumbs(modulePath, baseURL, packageChain, false)
		},
		"currentcrumb": func() string {
			l := len(packageChain)
			if l == 0 {
				return importPath
			}
			return packageChain[l-1]
		},
		"sections": func() []link {
			return nil
		},
	}).ParseFS(templatesFS, "templates/404.gohtml", "templates/layout.gohtml")
	if err != nil {
		return err
	}
	return t.Execute(out, struct {
		PageName   string
		ModulePath string
		ImportPath string
		Href       string
		Ref        string
	}{
		PageName:   "Not found",
		ModulePath: modulePath,
		ImportPath: importPath,
		Href:       moduleHref,
		Ref:        ref,
	})
}
