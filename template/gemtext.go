// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
// SPDX-FileCopyrightText: 2013 The Go Authors
//
// SPDX-License-Identifier: BSD-3-Clause

package template

import (
	"bytes"
	_ "embed"
	"go/ast"
	"io"
	"path"
	"strings"
	"text/template"
)

// GemtextRenderer provides the generation of [gemtext] documentation.
//
// [gemtext]: https://geminiprotocol.net/docs/gemtext-specification.gmi
type GemtextRenderer struct {
	BaseURL string
	DocSite string // defaults to "gemini://godocs.io"
}

// Execute applies a parsed template to the template data, and writes the output to w.
func (gr GemtextRenderer) Execute(out io.Writer, td TemplateData) error {
	domain, baseURL, found := strings.Cut(td.ModulePath, "/")
	if found {
		baseURL = "/" + baseURL
	}
	if gr.BaseURL != "" {
		baseURL = gr.BaseURL
	}
	goPrinter := newGoPrinter(td.FileSet)
	pkgFolder := strings.Join(td.PackageChain, "/")
	docSite := gr.DocSite
	if docSite == "" {
		docSite = "gemini://godocs.io"
	}
	t, err := template.New("index.gmi").Funcs(template.FuncMap{
		"comment": func(s string) string {
			comment := td.Package.Parser().Parse(s)
			return gemtext(comment, td.ModulePath, docSite, domain)
		},
		"dd": func(v any) (string, error) {
			buf := &bytes.Buffer{}
			err := ast.Fprint(buf, td.FileSet, v, ast.NotNilFilter)
			return buf.String(), err
		},
		"prettyPrintAST":    goPrinter.PrettyPrintAST,
		"formatExampleTest": goPrinter.ExampleTest,
		"fileForgeLink": func(name string) string {
			href := td.VCS.Forge.FileURL(path.Join(pkgFolder, name), td.Ref)
			if href == "" {
				return "* " + name
			}
			return "=> " + href + " " + name
		},
		"breadcrumbs": func() []link {
			links := breadcrumbs(td.ModulePath, baseURL, td.PackageChain, true)
			slicesReverse(links)
			return links
		},
		"subPackageURL": func(name string) string {
			return baseURL + "/" + strings.Join(append(td.PackageChain, name), "/")
		},
		"differentPackageName": func() bool {
			return td.Package.Name != "" && !strings.HasSuffix(td.Package.ImportPath, "/"+td.Package.Name)
		},
	}).ParseFS(templatesFS, "templates/index.gmi")
	if err != nil {
		return err
	}
	return t.Execute(out, td)
}

// Reverse reverses the elements of the slice in place.
// copied from go 1.21 (available starting with alpine v3.19)
func slicesReverse[S ~[]E, E any](s S) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
}
