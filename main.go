// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

/*
vanitydoc generates go documentation as a static site (HTML and Gemtext).
When served under the import path URL, it makes the package importable by the Go tools ("vanity import path").

For instance the import path for this command is code.pfad.fr/vanitydoc and the documentation is hosted
at the following URL: https://code.pfad.fr/vanitydoc

Usage:

	vanitydoc [flags] <path>
	# or using go run:     go run code.pfad.fr/vanitydoc@latest [flags] <path>
	# or using nix flakes: nix run git+https://code.pfad.fr/vanitydoc [flags] <path>

If the <path> argument points to a folder (or is omitted), a local HTTP server will be started to render the documentation on demand of the given folder (or the current folder), taking into account the following flags:

	-addr string
		listen address to expose the documentation (default "localhost:8080")
	-gomodcache string
		path to the GOMODCACHE folder, for pkg.go.dev-like experience when giving the stdlib path as <path> argument
	-html.template.pattern string
		pattern for additional HTML templates
	-html.template.root string
		root folder of the additional HTML templates (default ".")
	-modulePath string
		module path (will be read from <path>/go.mod if empty)

If the <path> argument points to a file (or "-" for stdin), the file is read as JSON, the configured modules are downloaded and their documentation generated.

# Example to generate this documentation

The path to the JSON file containing the configuration should be given as argument to the executable. See the README for [configuration reference].

	{
		"defaults": { // allows to specify defaults for all "modules" below
			// forgejo, gitsrht, hgsrht and github are supported
			"type": "forgejo",

			// the module name will be appended to construct the forge url
			"url_prefix": "https://codeberg.org/pfad.fr/",
			"default_branch": "main"
		},

		"modules": {
			"vanitydoc": {}, // use all defaults for the module hosted at "{url_prefix}/vanitydoc"
			"risefront": {
				"ref": "v1.0.0" // specify the ref to retrieve for this module
			},
			"fluhus/godoc-tricks": {
				"type": "github",
				"url": "https://github.com/fluhus/godoc-tricks",
				"default_branch": "master",
				"ref": "master",

				// github tweak to download the archive of a branch
				// (use "tags/" when "ref" is a tag)
				"archive_ref_prefix": "heads/"
			},
			"exp": {
				"type": "gitsrht",
				"url": "https://git.sr.ht/~oliverpool/exp"
			}
		},

		"html": {
			"output": "dist/http",

			// you can add your templates to customize the HTML
			"template.pattern": "header.gohtml",
			"first_clone_url_info_refs_path": "info/refs.redirect"
		},
		"gemtext": {
			"output": "dist/gmi"
		},

		"max_conns_per_host": 3 // be nice with the forge when downloading module archives
	}

# License and credits

Code is available under the BSD-3-Clause license.

Most of the templates originate from [gddo] (available under BSD-3-Clause).
The default CSS styling is inspired by [simple.css] (available under MIT).
[go-import] meta tag as well as the meta tags proposed by the [VCS Autodiscovery RFC] are included in the HTML output.

[gddo]: https://git.sr.ht/~sircmpwn/gddo
[simple.css]: https://simplecss.org/
[go-import]: https://pkg.go.dev/cmd/go#hdr-Remote_import_paths
[VCS Autodiscovery RFC]: https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc/tree/HEAD/RFC.md
[configuration reference]: https://codeberg.org/pfad.fr/vanitydoc#configuration
*/
package main

import (
	"bufio"
	"context"
	"errors"
	"flag"
	"fmt"
	"go/build"
	"io"
	"io/fs"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"

	"code.pfad.fr/vanitydoc/autodiscovery"
	"code.pfad.fr/vanitydoc/template"
)

func main() {
	modulePath := flag.String("modulePath", "", "module path (will be read from <path>/go.mod if empty)")
	serveAddr := flag.String("addr", "localhost:8080", "listen address to expose the documentation")
	templatePatternHTML := flag.String("html.template.pattern", "", "pattern for additional HTML templates")
	templateRootHTML := flag.String("html.template.root", ".", "root folder of the additional HTML templates")
	gomodcache := flag.String("gomodcache", "", "path to the GOMODCACHE folder, for pkg.go.dev-like experience when giving the stdlib path as <path> argument")
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "usage (generate from JSON): %s <path>\n", os.Args[0])
		fmt.Fprintln(flag.CommandLine.Output(), `  <path> filepath of a json describing the modules to generate the documentation for (stdin if set to a single dash "-")`)
		fmt.Fprintln(flag.CommandLine.Output())
		fmt.Fprintf(flag.CommandLine.Output(), "usage (serve on localhost): %s [flags] <path>\n", os.Args[0])
		fmt.Fprintln(flag.CommandLine.Output(), "  <path> folderpath of the module to serve the documentation for (current folder if omitted)")
		flag.PrintDefaults()
	}
	flag.Parse()
	htmlFlags := htmlRendererFlags{
		Favicon:         "/gopher.png",
		Stylesheets:     []string{"/vanitydoc.css"},
		TemplateRoot:    *templateRootHTML,
		TemplatePattern: *templatePatternHTML,
	}
	if err := mainWithFlags(context.Background(), *modulePath, flag.Arg(0), htmlFlags, *serveAddr, *gomodcache); err != nil {
		log.Fatal(err)
	}
}

func chdir(dir string) (func() error, error) {
	current, err := os.Getwd()
	if err != nil {
		return nil, fmt.Errorf("could not get current working dir: %w", err)
	}
	return func() error {
		return os.Chdir(current)
	}, os.Chdir(dir)
}

func mainWithFlags(ctx context.Context, modulePath, jsonPath string, htmlFlags htmlRendererFlags, serveAddr, gomodcache string) error {
	moduleDir := "."
	if jsonPath != "" {
		r := os.Stdin
		if jsonPath != "-" {
			fi, err := os.Stat(jsonPath)
			if err != nil {
				return err
			}

			if fi.IsDir() {
				r = nil // jsonPath is a dir: use it as moduleDir
				moduleDir = jsonPath
			} else {
				r, err = os.Open(jsonPath)
				if err != nil {
					return err
				}
				defer r.Close()

				if d := filepath.Dir(jsonPath); d != "." {
					undo, err := chdir(d)
					if err != nil {
						return err
					}
					defer undo()
				}
			}
		}
		if r != nil {
			return fromJSON(r)
		}
	}

	absDir, err := filepath.Abs(moduleDir)
	if err != nil {
		absDir = moduleDir
	}
	log.Println("Directory:", absDir)
	if modulePath == "" {
		var err error
		modulePath, err = findModulePath(os.DirFS(moduleDir))
		if err != nil {
			return fmt.Errorf("could not deduce modulePath for %s: %w", moduleDir, err)
		}
	}
	log.Println("Module:", modulePath)
	return serve(ctx, os.DirFS(moduleDir), modulePath, htmlFlags, serveAddr, gomodcache)
}

func findModulePath(fsys fs.FS) (string, error) {
	f, err := fsys.Open("go.mod")
	if err != nil {
		return "", err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		suffix, found := strings.CutPrefix(scanner.Text(), "module ")
		if found {
			suffix, _, _ = strings.Cut(suffix, " ")
			return suffix, nil
		}
	}
	if err := scanner.Err(); err != nil {
		return "", err
	}
	return "", errors.New("no 'module ' line found")
}

func pkgFromRoot(fsys fs.FS, modulePath string) ([]goPackage, error) {
	var gopkgs []goPackage
	pkgIndex := make(map[string]int)

	docs := make(map[string]string)
	buildContext := newBuildContext(fsys)

	err := fs.WalkDir(fsys, ".", func(name string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if !d.IsDir() {
			return nil
		}
		if name != "." {
			if d.Name()[0] == '.' { // skip hidden folder
				return filepath.SkipDir
			}
			if d.Name() == "testdata" { // skip testdata folder
				return filepath.SkipDir
			}
			_, err := fs.Stat(fsys, name+"/go.mod")
			if err == nil { // skip nested modules
				return filepath.SkipDir
			}
		}
		importPath := path.Join(modulePath, name)
		bpkg, err := buildContext.ImportDir(name, build.ImportComment|build.IgnoreVendor)
		if err != nil {
			if nogoerr := new(build.NoGoError); errors.As(err, &nogoerr) {
				if name != "." { // Keep root folder, even if it has no go file.
					return nil
				}
			} else {
				return err
			}
		}
		docs[importPath] = bpkg.Doc
		pkgIndex[importPath] = len(gopkgs)
		subFS, err := fs.Sub(fsys, name)
		if err != nil {
			return err
		}
		gopkgs = append(gopkgs, goPackage{
			FS:         subFS,
			ImportPath: importPath,
			ModulePath: modulePath,
			// Directories: // built below
			// Breadcrumbs: // built below
		})
		return nil
	})
	if err != nil {
		return nil, err
	}

	longestParent := func(s string) string {
		for s != "." {
			s = path.Dir(s)
			if _, ok := pkgIndex[s]; ok {
				return s
			}
		}
		return ""
	}
	for i, p := range gopkgs {
		parent := longestParent(p.ImportPath)
		if parent != "" {
			pp := gopkgs[pkgIndex[parent]]
			sub := p.ImportPath[len(parent)+1:]
			pp.Directories = append(pp.Directories, template.Directory{
				Name:     sub,
				Synopsis: docs[p.ImportPath],
			})
			gopkgs[pkgIndex[parent]] = pp
		}

		chain := []string{}
		previous := p.ModulePath
		current := ""
		for _, s := range strings.Split(p.ImportPath[len(p.ModulePath):], "/") {
			if current != "" {
				current += "/"
			}
			current += s
			if _, ok := pkgIndex[previous+"/"+current]; ok {
				chain = append(chain, current)
				previous += "/" + current
				current = ""
			}
		}
		if current != "" {
			chain = append(chain, current)
		}
		gopkgs[i].PackageChain = chain
	}

	return gopkgs, nil
}

func newBuildContext(fsys fs.FS) build.Context {
	ctx := build.Default

	ctx.IsDir = func(name string) bool {
		s, err := fs.Stat(fsys, name)
		if err != nil {
			return false
		}
		return s.IsDir()
	}
	ctx.ReadDir = func(dir string) ([]fs.FileInfo, error) {
		list, err := fs.ReadDir(fsys, dir)
		if err != nil {
			return nil, err
		}
		fi := make([]fs.FileInfo, 0, len(list))
		for _, d := range list {
			i, err := d.Info()
			if err != nil {
				return nil, err
			}
			fi = append(fi, i)
		}
		return fi, nil
	}
	ctx.OpenFile = func(path string) (io.ReadCloser, error) { return fsys.Open(path) }
	ctx.JoinPath = path.Join // fs.FS expects slash-separated paths on all platforms

	// If left nil, the default implementations of these read from disk,
	// which we do not want. None of these functions should be used
	// inside this function; it would be an internal error if they are.
	// Set them to non-nil values to catch if that happens.
	ctx.SplitPathList = func(s string) []string { return nil } // apparently called if GOROOT is empty
	ctx.HasSubdir = func(root string, dir string) (string, bool) {
		return dir, !strings.HasPrefix(dir, "/") // inside the fs, all "dir" will be relative to the root of the fs
	}
	return ctx
}

type goPackage struct {
	FS                     fs.FS
	ImportPath, ModulePath string
	Directories            []template.Directory
	PackageChain           []string
}

type htmlOptions struct {
	renderer     template.HTMLRenderer
	OutputFolder string
}
type gemtextOptions struct {
	renderer     template.GemtextRenderer
	OutputFolder string
}

func generate(gopkgs []goPackage, htmlOptions htmlOptions, gemtextOptions gemtextOptions, vcs autodiscovery.VCS, autodiscoveryRef string) error {
	for _, item := range gopkgs {
		rel, err := filepath.Rel(item.ModulePath, item.ImportPath)
		if err != nil {
			return err
		}
		td, err := template.NewTemplateData(
			newBuildContext(item.FS),
			item.ModulePath,
			item.PackageChain,
			item.Directories,
			vcs,
			autodiscoveryRef)
		if err != nil {
			return err
		}

		if htmlOptions.OutputFolder != "" {
			err = writeFile(td, filepath.Join(htmlOptions.OutputFolder, rel), "index.html", htmlOptions.renderer.Execute)
			if err != nil {
				return fmt.Errorf("package %s: %w", rel, err)
			}
		}

		if gemtextOptions.OutputFolder != "" {
			err = writeFile(td, filepath.Join(gemtextOptions.OutputFolder, rel), "index.gmi", gemtextOptions.renderer.Execute)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func writeFile(td template.TemplateData, folder, name string, generate func(io.Writer, template.TemplateData) error) error {
	err := os.MkdirAll(folder, os.ModePerm)
	if err != nil {
		return err
	}
	f, err := os.Create(filepath.Join(folder, name))
	if err != nil {
		return err
	}
	defer f.Close()
	err = generate(f, td)
	if err != nil {
		return err
	}
	return f.Close()
}

type htmlRendererFlags struct {
	Favicon         string   `json:"favicon"`
	Stylesheets     []string `json:"stylesheet_links"`
	TemplateRoot    string   `json:"template.root"`
	TemplatePattern string   `json:"template.pattern"`
}

func (hf htmlRendererFlags) renderer(baseURL, favicon string) template.HTMLRenderer {
	hr := template.HTMLRenderer{
		BaseURL:     baseURL,
		Favicon:     firstNonEmpty(favicon, hf.Favicon),
		Stylesheets: hf.Stylesheets,
	}
	if hf.TemplatePattern != "" {
		hr.AdditionalPatterns = []string{hf.TemplatePattern}
		hr.AdditionalFS = os.DirFS(firstNonEmpty(hf.TemplateRoot, "."))
	}
	return hr
}
