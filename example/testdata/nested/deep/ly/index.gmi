# ly package - example.com/nested
```
import "example.com/nested/deep/ly"
```

Deeply nested package

## Constants

```
const Deeply = 42
```

## Functions

### func Trutle
```
func Trutle(turtle.Typ)
```

## Files

=> https://git.example.com/~username/nested/tree/main/item/deep/ly/ly.go ly.go
## Directories

=> /nested/deep/ly/turtle/ turtle
All the way down

## Breadcrumb
=> /nested/ example.com/nested

