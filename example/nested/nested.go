// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

// Package nested is a nested package for demo purposes.
package nested

import "example.com/nested/sub"

// Empty is not full
type Empty struct{}
type Full struct {
	// Field has some <info/>
	Field  string
	Nested struct { // nested
		Sub interface {
			unexported()
		}
	} // end-nested <strong>HTML</strong>
	Empty                      Empty               // is likely empty
	VeryEmptyLongToCheckIndent map[string]struct{} // of comment

	Full2      *Full
	unexported int
	Full       *Full
}

type Tags struct {
	// Field has some <info/>
	Field  string   `json:"field"`
	Nested struct { // nested
		Sub interface {
			unexported()
		}
	} `json:"nested"<info/>` // end-nested <strong>HTML</strong>
	Empty                      Empty               `json:"empty,omitempty"` // is likely empty
	VeryEmptyLongToCheckIndent map[string]struct{} // of comment

	Full2      *Full
	unexported int
	Full       *Full
}

const Contant = 42

var Variable = sub.Marine{
	Depth: 42,
}
var Variable2 = &sub.Marine{
	Depth: 42,
	Speed: -1,
}
var Variable3 = &sub.Marine{Depth: 42, Speed: -1}
var Variable4 = Full{
	Field:  "f",
	Nested: struct{ Sub interface{ unexported() } }{},
	Empty:  Empty{},
	VeryEmptyLongToCheckIndent: map[string]struct{}{
		"answer": {},
	},

	Full:       &Full{unexported: 0},
	unexported: 0,
}
var Variable5 = Full{
	Field:                      "f",
	Nested:                     struct{ Sub interface{ unexported() } }{},
	Empty:                      Empty{},
	VeryEmptyLongToCheckIndent: map[string]struct{}{
		// "answer": {},
	},

	Full: &Full{
		unexported: 0,
	},
	unexported: 0,
}

var Array = [...]int{1, 2, 3}[1]
var Fun = Func(1)
var FunGen = FuncGen[int]([]int{12, 0}...)

func Func(int) int {
	return 42
}

func FuncGen[T ~int](n ...T) int {
	return int(n[0]) + 1
}

type Interface interface{}

func FuncGenIndex[T ~int, V float64](n ...T) int {
	return int(n[0]) + 1
}

var FunGenInd = FuncGenIndex[int, float64]([]int{12, 0}...)

const MultiLineConst = "first" +
	"second" + "third" +
	"fourth"
const SingleLineAddition = 1 + 2 + 3
