// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package nested

type Emitting <-chan int
type Receiving chan<- int
type Chan chan int

var C Chan = nil

func Transmit(ch <-chan int, c chan chan struct{}) chan<- int {
	return nil
}

func (Chan) NoName() {}

type Reader struct {
	unexportedField int
}

func (r Reader) unexported() int {
	return 0
}

type Writer interface {
	unexported() int
}

var MathExpr = (1+2+1 == 3) || !true
var Indexed = []int{1, 2, 3}[1:2]
var Indexed2 = []int{1, 2, 3}[1:2:3]
var Indexed3 = []int{1, 2, 3}[:1:3]
var Indexed4 = []int{1, 2, 3}[:]
var w Writer
var AssertedReader, ok = w.(Reader)

var InlineFunc = func(n int) int {
	// <hr/>
	_ = "<hr/>"
	return n + 1
}
