// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package nested_test

func add(a, b int) int {
	return a + b
}

// <strong>do not try this at home</strong>

func Example_play() {
	if add(1, 1) != 2 {
		panic("houston, we have a problem")
	}
}
