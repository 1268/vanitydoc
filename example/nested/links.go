// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package nested

import (
	"crypto/tls"
	"net/http"

	"git.sr.ht/~adnano/go-gemini"
)

type Server struct {
	GetCertificate func(*tls.ClientHelloInfo) (*tls.Certificate, error)

	HttpHandlers   map[string]http.Handler   // the keys must be punycode encoded, see [ToPunyCode]
	GeminiHandlers map[string]gemini.Handler // the keys must be punycode encoded, see [ToPunyCode]
}
