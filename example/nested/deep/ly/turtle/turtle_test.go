// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package turtle

import "fmt"

func ExampleTyp_Method() {
	fmt.Println("Answer:", Typ{}.Method())
}
