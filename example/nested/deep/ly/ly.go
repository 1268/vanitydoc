// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

// Deeply nested package
package ly

import "example.com/nested/deep/ly/turtle"

const Deeply = 42

func Trutle(turtle.Typ) {

}
