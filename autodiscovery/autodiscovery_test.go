// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package autodiscovery

import (
	"bytes"
	"reflect"
	"strings"
	"testing"
)

func TestSourceHutGit(t *testing.T) {
	ad := SourceHutGit("git.sr.ht/~ancarda/vcs-autodiscovery-rfc", "master")
	buf := &bytes.Buffer{}
	err := ad.MetaTags(buf)
	if err != nil {
		t.Fatal(err)
	}
	lines := strings.Split(buf.String(), "\n")
	expected := []string{
		`<meta name="vcs" content="git"/>`,
		`<meta name="vcs:default-branch" content="master"/>`,
		`<meta name="vcs:clone" content="https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc"/>`,
		`<meta name="vcs:clone" content="git@git.sr.ht:~ancarda/vcs-autodiscovery-rfc"/>`,
		`<meta name="forge:summary" content="https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc"/>`,
		`<meta name="forge:dir" content="https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc/tree/{ref}/item/{path}"/>`,
		`<meta name="forge:file" content="https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc/tree/{ref}/item/{path}"/>`,
		`<meta name="forge:rawfile" content="https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc/blob/{ref}/{path}"/>`,
		`<meta name="forge:line" content="https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc/tree/{ref}/item/{path}#L{line}"/>`,
	}
	if len(lines) < len(expected) {
		t.Errorf("expected %d lines, got: %d", len(expected), len(lines))
	}
	for i, got := range lines {
		if i >= len(expected) {
			break
		}
		if strings.TrimSpace(got) != expected[i] {
			t.Errorf("unexpected line %d, got:\n%s\n, expected:\n%s", i, got, expected[i])
		}
	}

	buf.Reset()
	err = ad.GoImport(buf, "git.sr.ht/~ancarda/vcs-autodiscovery-rfc")
	if err != nil {
		t.Fatal(err)
	}
	got := strings.TrimSpace(buf.String())
	goImport := `<meta name="go-import" content="git.sr.ht/~ancarda/vcs-autodiscovery-rfc git https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc"/>`
	if got != goImport {
		t.Errorf("expected go-import \n%s\n, got:\n%s", goImport, got)
	}

	got, want := ad.Forge.DirURL("docs/", "v0.0.0"), "https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc/tree/v0.0.0/item/docs/"
	if got != want {
		t.Errorf("DirURL() = %v, expected %v", got, want)
	}
	got, want = ad.Forge.FileURL("docs/README.md", "v0.0.0"), "https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc/tree/v0.0.0/item/docs/README.md"
	if got != want {
		t.Errorf("FileURL() = %v, expected %v", got, want)
	}
	got, want = ad.Forge.LineURL("docs/README.md", "v0.0.0", "1-9"), "https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc/tree/v0.0.0/item/docs/README.md#L1-9"
	if got != want {
		t.Errorf("LineURL() = %v, expected %v", got, want)
	}
	got, want = ad.Forge.RawFileURL("docs/README.md", "v0.0.0"), "https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc/blob/v0.0.0/docs/README.md"
	if got != want {
		t.Errorf("LineURL() = %v, expected %v", got, want)
	}
}

func TestNew(t *testing.T) {
	cases := []struct {
		typ, url, defaultBranch string
		vcs                     VCS
	}{
		{
			// known scheme
			typ: "gitsrht",
			url: "git.example.com/~username/repo",
			vcs: VCS{
				Kind:          "git",
				DefaultBranch: "main",
				Clone: []string{
					"https://git.example.com/~username/repo",
					"git@git.example.com:~username/repo",
				},
				Forge: Forge{
					RawFile: "https://git.example.com/~username/repo" + "/blob/{ref}/{path}",
					File:    "https://git.example.com/~username/repo" + "/tree/{ref}/item/{path}",
					Dir:     "https://git.example.com/~username/repo" + "/tree/{ref}/item/{path}",
					Summary: "https://git.example.com/~username/repo",
					Line:    "https://git.example.com/~username/repo" + "/tree/{ref}/item/{path}#L{line}",
				},
			},
		},
		{
			// empty path
			typ: "gitsrht",
			url: "git.example.com",
			vcs: VCS{
				Kind:          "git",
				DefaultBranch: "main",
				Clone: []string{
					"https://git.example.com",
					"git@git.example.com:",
				},
				Forge: Forge{
					RawFile: "https://git.example.com" + "/blob/{ref}/{path}",
					File:    "https://git.example.com" + "/tree/{ref}/item/{path}",
					Dir:     "https://git.example.com" + "/tree/{ref}/item/{path}",
					Summary: "https://git.example.com",
					Line:    "https://git.example.com" + "/tree/{ref}/item/{path}#L{line}",
				},
			},
		},
		{
			// specific branch
			typ:           "gitsrht",
			url:           "git.example.com/~username/repo",
			defaultBranch: "dev",
			vcs: VCS{
				Kind:          "git",
				DefaultBranch: "dev",
				Clone: []string{
					"https://git.example.com/~username/repo",
					"git@git.example.com:~username/repo",
				},
				Forge: Forge{
					RawFile: "https://git.example.com/~username/repo" + "/blob/{ref}/{path}",
					File:    "https://git.example.com/~username/repo" + "/tree/{ref}/item/{path}",
					Dir:     "https://git.example.com/~username/repo" + "/tree/{ref}/item/{path}",
					Summary: "https://git.example.com/~username/repo",
					Line:    "https://git.example.com/~username/repo" + "/tree/{ref}/item/{path}#L{line}",
				},
			},
		},
		{
			// known scheme
			typ: "forgejo",
			url: "forgejo.example.com/~username/repo",
			vcs: VCS{
				Kind:          "git",
				DefaultBranch: "main",
				Clone: []string{
					"https://forgejo.example.com/~username/repo.git",
					"git@forgejo.example.com:~username/repo.git",
				},
				Forge: Forge{
					RawFile: "https://forgejo.example.com/~username/repo" + "/raw/{ref}/{path}",
					File:    "https://forgejo.example.com/~username/repo" + "/src/{ref}/{path}",
					Dir:     "https://forgejo.example.com/~username/repo" + "/src/{ref}/{path}",
					Summary: "https://forgejo.example.com/~username/repo",
					Line:    "https://forgejo.example.com/~username/repo" + "/src/{ref}/{path}#L{line}",
				},
			},
		},
		{
			// this repo
			typ: "forgejo",
			url: "codeberg.org/pfad.fr/vanitydoc",
			vcs: Forgejo("https://codeberg.org/pfad.fr/vanitydoc", "main"),
		},
		{
			typ: "github",
			url: "github.com/fluhus/godoc-tricks",
			vcs: Github("https://github.com/fluhus/godoc-tricks", "main"),
		},
		{
			typ: "hgsrht",
			url: "hg.sr.ht/~scoopta/wofi",
			vcs: SourceHutHg("https://hg.sr.ht/~scoopta/wofi", "tip"),
		},
	}
	for _, c := range cases {
		t.Run(c.typ+"/"+c.url, func(t *testing.T) {
			vcs, err := New(c.typ, c.url, c.defaultBranch)
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(vcs, c.vcs) {
				t.Fatalf("got %v, expected: %v", vcs, c.vcs)
			}
		})
	}
}

func TestInfer(t *testing.T) {
	cases := []struct {
		url string
		vcs VCS
	}{
		{
			url: "https://git.sr.ht/~scoopta/wofi",
			vcs: SourceHutGit("https://git.sr.ht/~scoopta/wofi", "main"),
		},
		{
			url: "https://hg.sr.ht/~scoopta/wofi",
			vcs: SourceHutHg("https://hg.sr.ht/~scoopta/wofi", "tip"),
		},
		{
			url: "https://codeberg.org/pfad.fr/vanitydoc",
			vcs: Forgejo("https://codeberg.org/pfad.fr/vanitydoc", "main"),
		},
		{
			url: "https://github.com/fluhus/godoc-tricks",
			vcs: Github("https://github.com/fluhus/godoc-tricks", "main"),
		},
	}
	for _, c := range cases {
		t.Run(c.url, func(t *testing.T) {
			vcs, err := Infer(c.url)
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(vcs, c.vcs) {
				t.Fatalf("got %v, expected: %v", vcs, c.vcs)
			}
		})
	}
}

func TestTypeError(t *testing.T) {
	for _, typ := range []string{
		"",
		"nonexisting",
	} {
		t.Run(typ, func(t *testing.T) {
			_, err := New(typ, "", "")
			if err == nil {
				t.Fatal("expected an error")
			}
		})
	}
}

func TestSourceHutHg(t *testing.T) {
	ad := SourceHutHg("hg.sr.ht/~scoopta/wofi", "tip")
	buf := &bytes.Buffer{}
	err := ad.MetaTags(buf)
	if err != nil {
		t.Fatal(err)
	}
	lines := strings.Split(buf.String(), "\n")
	expected := []string{
		`<meta name="vcs" content="hg"/>`,
		`<meta name="vcs:default-branch" content="tip"/>`,
		`<meta name="vcs:clone" content="https://hg.sr.ht/~scoopta/wofi"/>`,
		`<meta name="vcs:clone" content="ssh://hg@hg.sr.ht/~scoopta/wofi"/>`,
		`<meta name="forge:summary" content="https://hg.sr.ht/~scoopta/wofi"/>`,
		`<meta name="forge:dir" content="https://hg.sr.ht/~scoopta/wofi/tree/{ref}/item/{path}"/>`,
		`<meta name="forge:file" content="https://hg.sr.ht/~scoopta/wofi/tree/{ref}/item/{path}"/>`,
		`<meta name="forge:rawfile" content="https://hg.sr.ht/~scoopta/wofi/blob/{ref}/{path}"/>`,
		`<meta name="forge:line" content="https://hg.sr.ht/~scoopta/wofi/tree/{ref}/item/{path}#L{line}"/>`,
	}
	if len(lines) < len(expected) {
		t.Errorf("expected %d lines, got: %d", len(expected), len(lines))
	}
	for i, got := range lines {
		if i >= len(expected) {
			break
		}
		if strings.TrimSpace(got) != expected[i] {
			t.Errorf("unexpected line %d, got:\n%s\n, expected:\n%s", i, got, expected[i])
		}
	}

	buf.Reset()
	err = ad.GoImport(buf, "hg.sr.ht/~scoopta/wofi")
	if err != nil {
		t.Fatal(err)
	}
	got := strings.TrimSpace(buf.String())
	goImport := `<meta name="go-import" content="hg.sr.ht/~scoopta/wofi hg https://hg.sr.ht/~scoopta/wofi"/>`
	if got != goImport {
		t.Errorf("expected go-import \n%s\n, got:\n%s", goImport, got)
	}

	got, want := ad.Forge.DirURL("docs/", "v0.0.0"), "https://hg.sr.ht/~scoopta/wofi/tree/v0.0.0/item/docs/"
	if got != want {
		t.Errorf("DirURL() = %v, expected %v", got, want)
	}
	got, want = ad.Forge.FileURL("docs/README.md", "v0.0.0"), "https://hg.sr.ht/~scoopta/wofi/tree/v0.0.0/item/docs/README.md"
	if got != want {
		t.Errorf("FileURL() = %v, expected %v", got, want)
	}
	got, want = ad.Forge.LineURL("docs/README.md", "v0.0.0", "1-9"), "https://hg.sr.ht/~scoopta/wofi/tree/v0.0.0/item/docs/README.md#L1-9"
	if got != want {
		t.Errorf("LineURL() = %v, expected %v", got, want)
	}
	got, want = ad.Forge.RawFileURL("docs/README.md", "v0.0.0"), "https://hg.sr.ht/~scoopta/wofi/blob/v0.0.0/docs/README.md"
	if got != want {
		t.Errorf("LineURL() = %v, expected %v", got, want)
	}
}

func TestGithubGoSource(t *testing.T) {
	v := Github("https://github.com/go-yaml/yaml", "main")
	buf := &bytes.Buffer{}
	err := v.GoSource(buf, "gopkg.in/yaml.v2")
	if err != nil {
		t.Fatal(err)
	}
	got := strings.TrimSpace(buf.String())
	goImport := `<meta name="go-source" content="gopkg.in/yaml.v2 https://github.com/go-yaml/yaml https://github.com/go-yaml/yaml/blob/{commit}/{file} https://github.com/go-yaml/yaml/blob/{commit}/{file}#L{line}"/>`
	if got != goImport {
		t.Errorf("expected go-import \n%s\n, got:\n%s", goImport, got)
	}
}

func TestFirstNonEmpty(t *testing.T) {
	if firstNonEmpty() != "" {
		t.Fatal("unexpected firstNonEmpty")
	}
	if firstNonEmpty("a") != "a" {
		t.Fatal("unexpected firstNonEmpty")
	}
	if firstNonEmpty("", "a") != "a" {
		t.Fatal("unexpected firstNonEmpty")
	}
	if firstNonEmpty("a", "") != "a" {
		t.Fatal("unexpected firstNonEmpty")
	}
	if firstNonEmpty("a", "b") != "a" {
		t.Fatal("unexpected firstNonEmpty")
	}
}
