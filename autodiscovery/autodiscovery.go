// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

// package autodiscovery is an implementation of the [VCS Autodiscovery RFC].
//
// [VCS Autodiscovery RFC]: https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc/tree/HEAD/RFC.md
package autodiscovery

import (
	"fmt"
	"html/template"
	"io"
	"sort"
	"strings"
)

// VCS represents the Version Control System being used.
type VCS struct {
	// VCS Standard Tags
	Kind          string // git, fossil, pijul...
	DefaultBranch string
	Clone         []string // URLs for clones (unauthenticated first)
	Forge         Forge
}

// Forge indicates the URLs pattern available in the software forge (with {ref}, {path} and {line} as placeholders).
type Forge struct {
	Summary string // overview of the project
	Dir     string
	File    string // file with pretty printing
	Line    string // file with specific line hightlighted
	RawFile string
}

func (f Forge) DirURL(path, ref string) (url string) {
	url = strings.ReplaceAll(f.Dir, "{path}", path)
	return strings.ReplaceAll(url, "{ref}", ref)
}

func (f Forge) FileURL(path, ref string) (url string) {
	url = strings.ReplaceAll(f.File, "{path}", path)
	return strings.ReplaceAll(url, "{ref}", ref)
}

func (f Forge) LineURL(path, ref, line string) (url string) {
	url = strings.ReplaceAll(f.Line, "{path}", path)
	url = strings.ReplaceAll(url, "{ref}", ref)
	return strings.ReplaceAll(url, "{line}", line)
}

func (f Forge) RawFileURL(path, ref string) (url string) {
	url = strings.ReplaceAll(f.RawFile, "{path}", path)
	return strings.ReplaceAll(url, "{ref}", ref)
}

// Infer tries to infer the VCS from the clone url (not reliable, use at you own risks!)
func Infer(cloneURL string) (VCS, error) {
	var typ, suffix string
	switch {
	case strings.HasPrefix(cloneURL, "https://git.sr.ht/"):
		typ, suffix = "gitsrht", ""
	case strings.HasPrefix(cloneURL, "https://hg.sr.ht/"):
		typ, suffix = "hgsrht", ""
	case strings.HasPrefix(cloneURL, "https://codeberg.org/"):
		typ, suffix = "forgejo", ".git"
	case strings.HasPrefix(cloneURL, "https://github.com/"):
		typ, suffix = "github", ".git"
		// case strings.HasSuffix(cloneURL, ".git"):
		// 	typ, suffix = "forgejo", ".git"
	}
	cloneURL, _ = strings.CutSuffix(cloneURL, suffix)
	return New(typ, cloneURL, "")
}

// New creates a VCS of the given type (forgejo, gitsrht, hgsrht, github).
func New(typ string, url, defaultBranch string) (VCS, error) {
	knownScheme := map[string]func(url, defaultBranch string) VCS{
		"gitsrht": SourceHutGit,
		"hgsrht":  SourceHutHg,
		"forgejo": Forgejo,
		"gitea":   Forgejo,
		"github":  Github,
	}
	generator := knownScheme[typ]
	if generator == nil {
		supportedScheme := make([]string, 0, len(knownScheme))
		for s := range knownScheme {
			supportedScheme = append(supportedScheme, s)
		}
		sort.StringSlice(supportedScheme).Sort()
		return VCS{}, fmt.Errorf("%q is not a supported forge type: %s", typ, supportedScheme)
	}
	return generator(url, defaultBranch), nil
}

var metaTemplate = template.Must(template.New("name").Parse(`<meta name="{{.Name}}" content="{{.Content}}"/>` + "\n"))

// MetaTags writes the [VCS Autodiscovery RFC] meta tags (which are not empty).
//
// [VCS Autodiscovery RFC]: https://git.sr.ht/~ancarda/vcs-autodiscovery-rfc/tree/HEAD/RFC.md
func (vcs VCS) MetaTags(wr io.Writer) error {
	var err error
	writeMetaIfNotEmpty := func(name, content string) {
		if err != nil || content == "" {
			return
		}
		err = metaTemplate.Execute(wr, struct {
			Name, Content string
		}{
			Name:    name,
			Content: content,
		})
	}

	writeMetaIfNotEmpty("vcs", vcs.Kind)
	writeMetaIfNotEmpty("vcs:default-branch", vcs.DefaultBranch)
	for _, clone := range vcs.Clone {
		writeMetaIfNotEmpty("vcs:clone", clone)
	}

	writeMetaIfNotEmpty("forge:summary", vcs.Forge.Summary)
	writeMetaIfNotEmpty("forge:dir", vcs.Forge.Dir)
	writeMetaIfNotEmpty("forge:file", vcs.Forge.File)
	writeMetaIfNotEmpty("forge:rawfile", vcs.Forge.RawFile)
	writeMetaIfNotEmpty("forge:line", vcs.Forge.Line)

	return err
}

// GoImport writes the [go-import] meta tag.
//
// [go-import]: https://go.dev/ref/mod#vcs-find
func (vcs VCS) GoImport(wr io.Writer, importPrefix string) error {
	for _, clone := range vcs.Clone {
		// return early, since only one tag should be present
		return metaTemplate.Execute(wr, struct {
			Name, Content string
		}{
			Name:    "go-import",
			Content: importPrefix + " " + vcs.Kind + " " + clone,
		})
	}
	return nil
}

// GoSource writes the [go-source] meta tag. Its format officially does not contain the version
// however its seem to be used by [pkgsite] as {commit}.
//
// [pkgsite]: https://github.com/golang/pkgsite/blob/8984be28e84911c8a2f2c2e43ccee127bb87259d/internal/source/source.go#L537
// [go-source]: https://github.com/golang/pkgsite/blob/master/internal/source/meta-tags.go#L119
func (vcs VCS) GoSource(wr io.Writer, importPrefix string) error {
	return metaTemplate.Execute(wr, struct {
		Name, Content string
	}{
		Name:    "go-source",
		Content: importPrefix + " " + vcs.Forge.Summary + " " + vcs.Forge.DirURL("{file}", "{commit}") + " " + vcs.Forge.LineURL("{file}", "{commit}", "{line}"),
	})
}

// SourceHutGit generates VCS for git.sr.ht.
// For instance "https://git.sr.ht/~username/repo" and defaultBranch "main".
func SourceHutGit(url, defaultBranch string) VCS {
	httpURL, sshURL := commonGitCloneURLs(url)
	return VCS{
		Kind:          "git",
		DefaultBranch: firstNonEmpty(defaultBranch, "main"),
		Clone: []string{
			httpURL,
			sshURL,
		},
		Forge: Forge{
			RawFile: httpURL + "/blob/{ref}/{path}",
			File:    httpURL + "/tree/{ref}/item/{path}",
			Dir:     httpURL + "/tree/{ref}/item/{path}",
			Summary: httpURL,
			Line:    httpURL + "/tree/{ref}/item/{path}#L{line}",
		},
	}
}

// Forgejo generates VCS for forgejo/gitea.
// For instance "https://codeberg.org/pfad.fr/vanitydoc" and defaultBranch "main".
func Forgejo(url, defaultBranch string) VCS {
	httpURL, sshURL := commonGitCloneURLs(url)
	return VCS{
		Kind:          "git",
		DefaultBranch: firstNonEmpty(defaultBranch, "main"),
		Clone: []string{
			httpURL + ".git",
			sshURL + ".git",
		},
		Forge: Forge{
			// {ref} is not quite correct on Forgejo
			// it uses "commit/sha1" and "branch/main"
			// but it redirects them properly, so I guess this is fine
			RawFile: httpURL + "/raw/{ref}/{path}",
			File:    httpURL + "/src/{ref}/{path}",
			Dir:     httpURL + "/src/{ref}/{path}",
			Summary: httpURL,
			Line:    httpURL + "/src/{ref}/{path}#L{line}",
		},
	}
}

// Github generates VCS for github.
// For instance "https://github.com/fluhus/godoc-tricks" and defaultBranch "main".
func Github(url, defaultBranch string) VCS {
	httpURL, sshURL := commonGitCloneURLs(url)
	return VCS{
		Kind:          "git",
		DefaultBranch: firstNonEmpty(defaultBranch, "main"),
		Clone: []string{
			httpURL + ".git",
			sshURL + ".git",
		},
		Forge: Forge{
			RawFile: httpURL + "/raw/{ref}/{path}",
			File:    httpURL + "/blob/{ref}/{path}",
			Dir:     httpURL + "/blob/{ref}/{path}",
			Summary: httpURL,
			Line:    httpURL + "/blob/{ref}/{path}#L{line}",
		},
	}
}

func commonGitCloneURLs(url string) (http string, ssh string) {
	url = strings.TrimRight(url, "/")
	if !strings.Contains(url, "://") {
		url = "https://" + url
	}
	_, suffix, _ := strings.Cut(url, "://")
	domain, uri, _ := strings.Cut(suffix, "/")

	return url, "git@" + domain + ":" + uri
}

// SourceHutHg generates VCS for hg.sr.ht.
// For instance "https://hg.sr.ht/~username/repo" and defaultBranch "tip".
func SourceHutHg(url, defaultBranch string) VCS {
	httpURL, sshURL := commonHgCloneURLs(url)
	return VCS{
		Kind:          "hg",
		DefaultBranch: firstNonEmpty(defaultBranch, "tip"),
		Clone: []string{
			httpURL,
			sshURL,
		},
		Forge: Forge{
			RawFile: httpURL + "/blob/{ref}/{path}",
			File:    httpURL + "/tree/{ref}/item/{path}",
			Dir:     httpURL + "/tree/{ref}/item/{path}",
			Summary: httpURL,
			Line:    httpURL + "/tree/{ref}/item/{path}#L{line}",
		},
	}
}

func commonHgCloneURLs(url string) (http string, ssh string) {
	url = strings.TrimRight(url, "/")
	if !strings.Contains(url, "://") {
		url = "https://" + url
	}
	_, suffix, _ := strings.Cut(url, "://")
	domain, uri, _ := strings.Cut(suffix, "/")

	return url, "ssh://hg@" + domain + "/" + uri
}

func firstNonEmpty(str ...string) string {
	for _, s := range str {
		if s != "" {
			return s
		}
	}
	return ""
}
