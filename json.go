// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package main

import (
	"archive/tar"
	"archive/zip"
	"compress/gzip"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"log/slog"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"

	"code.pfad.fr/vanitydoc/autodiscovery"
	"code.pfad.fr/vanitydoc/template"
)

func firstNonEmpty(str ...string) string {
	for _, s := range str {
		if s != "" {
			return s
		}
	}
	return ""
}

func removeVersionSuffix(name string) string {
	i := strings.LastIndex(name, "/v")
	if i < 0 {
		return name
	}
	v, err := strconv.Atoi(name[i+2:])
	if err != nil || v <= 1 { // v1 is usually invalid
		return name
	}

	return name[:i]
}

func fromJSON(r io.Reader) error {
	var vanityServer struct {
		HTML struct {
			htmlRendererFlags
			OutputFolder              string `json:"output"`
			FirstCloneURLInfoRefsPath string `json:"first_clone_url_info_refs_path"`
		} `json:"html"`
		MaxConnsPerHost int `json:"max_conns_per_host"`
		Gemtext         struct {
			Output string `json:"output"`
		} `json:"gemtext"`
		Defaults moduleDefaults    `json:"defaults"`
		Modules  map[string]module `json:"modules"`
	}
	err := json.NewDecoder(r).Decode(&vanityServer)
	if err != nil {
		return fmt.Errorf("could not parse JSON: %w", err)
	}

	transport := http.DefaultTransport.(*http.Transport).Clone()
	transport.MaxConnsPerHost = vanityServer.MaxConnsPerHost
	client := &http.Client{
		Transport: transport,
	}

	tmpDir, err := os.MkdirTemp("", "vanitydoc-")
	if err != nil {
		return fmt.Errorf("could not create tmp dir: %w", err)
	}
	defer os.RemoveAll(tmpDir)

	defaults := vanityServer.Defaults
	generateModule := func(m module) error {
		var err error
		m.vcs, err = autodiscovery.New(
			firstNonEmpty(m.Type, defaults.Type),
			firstNonEmpty(m.URL, defaults.URLPrefix+removeVersionSuffix(m.name)),
			firstNonEmpty(m.DefaultBranch, defaults.DefaultBranch),
		)
		if err != nil {
			return err
		}
		m.populateArchiveURL(defaults)

		zFS, err := m.downloadZIP(client, tmpDir)
		if err != nil {
			return fmt.Errorf("could not download: %w", err)
		}
		defer zFS.Close()

		entries, err := fs.ReadDir(zFS, ".")
		if err != nil {
			return err
		}
		if len(entries) != 1 || !entries[0].IsDir() {
			return fmt.Errorf("expected exactly one dir entry in zip, got: %v", entries)
		}
		fsys, err := fs.Sub(zFS, entries[0].Name())
		if err != nil {
			return err
		}

		modulePath, err := findModulePath(fsys)
		if err != nil {
			return err
		}

		if !strings.HasSuffix(modulePath, m.name) {
			slog.Info("suspicious module name (module path does not ends with the provided module name)", "modulePath", modulePath, "name", m.name)
		}

		htmlOptions := htmlOptions{
			renderer: vanityServer.HTML.renderer("/"+m.name, m.Favicon),
		}
		if len(htmlOptions.renderer.Stylesheets) == 0 {
			htmlOptions.renderer.Stylesheets = []string{"/vanitydoc.css"}
		}
		if vanityServer.HTML.OutputFolder != "" {
			htmlOptions.OutputFolder = filepath.Join(vanityServer.HTML.OutputFolder, m.name)

			if gitRedirectFile := vanityServer.HTML.FirstCloneURLInfoRefsPath; gitRedirectFile != "" {
				gitRedirectFile = filepath.Join(htmlOptions.OutputFolder, gitRedirectFile)
				if err := os.MkdirAll(filepath.Dir(gitRedirectFile), 0o777); err != nil {
					return err
				}
				if err := os.WriteFile(gitRedirectFile, []byte(m.vcs.Clone[0]+"/info/refs"), 0o666); err != nil {
					return err
				}
			}
		}
		gemtextOptions := gemtextOptions{}
		if vanityServer.Gemtext.Output != "" {
			gemtextOptions.OutputFolder = filepath.Join(vanityServer.Gemtext.Output, m.name)
			gemtextOptions.renderer.BaseURL = "/" + m.name
		}

		gopkgs, err := pkgFromRoot(fsys, modulePath)
		if err != nil {
			return err
		}
		autodiscoveryRef := firstNonEmpty(m.Ref, defaults.Ref, m.vcs.DefaultBranch)
		err = generate(gopkgs, htmlOptions, gemtextOptions, m.vcs, autodiscoveryRef)
		if err != nil {
			return err
		}

		return nil
	}

	var wg sync.WaitGroup
	i := 0
	errs := make([]error, len(vanityServer.Modules))
	for name, mo := range vanityServer.Modules {
		mo.name = name
		j := i
		i++
		wg.Add(1)
		go func(m module) {
			defer wg.Done()
			err := generateModule(m)
			if err != nil {
				errs[j] = fmt.Errorf("%s: %w", m.name, err)
			}
		}(mo)
	}

	// write vanitydoc.css
	if dir := vanityServer.HTML.OutputFolder; dir != "" && len(vanityServer.HTML.Stylesheets) == 0 {
		if err := os.MkdirAll(dir, 0o777); err != nil {
			return err
		}

		f, err := os.Create(filepath.Join(dir, "vanitydoc.css"))
		if err != nil {
			return err
		}
		defer f.Close()
		if err := template.WriteCSS(f); err != nil {
			return err
		}
		if err := f.Close(); err != nil {
			return err
		}
	}

	wg.Wait()
	return errors.Join(errs...)
}

type module struct {
	name string
	vcs  autodiscovery.VCS

	Type             string  `json:"type"`
	URL              string  `json:"url"`
	DefaultBranch    string  `json:"default_branch"`
	Ref              string  `json:"ref"`
	ArchiveRefPrefix *string `json:"archive_ref_prefix"`

	ArchiveURL     string `json:"archive_url"`
	ArchiveIsTarGz bool   `json:"archive_targz"`

	Favicon string `json:"html_favicon"`
}

type moduleDefaults struct {
	Type             string `json:"type"`
	URLPrefix        string `json:"url_prefix"`
	DefaultBranch    string `json:"default_branch"`
	Ref              string `json:"ref"`
	ArchiveRefPrefix string `json:"archive_ref_prefix"`
}

func (m *module) populateArchiveURL(defaults moduleDefaults) {
	if m.ArchiveURL != "" {
		return
	}
	refPrefix := defaults.ArchiveRefPrefix
	if m.ArchiveRefPrefix != nil {
		refPrefix = *m.ArchiveRefPrefix
	}
	m.ArchiveURL, m.ArchiveIsTarGz = archiveURL(
		firstNonEmpty(m.Type, defaults.Type),
		m.vcs.Forge.Summary,
		refPrefix+firstNonEmpty(m.Ref, defaults.Ref, m.vcs.DefaultBranch),
	)
	if m.ArchiveURL == "" {
		panic("unsupported forge type: " + firstNonEmpty(m.Type, defaults.Type))
	}
}

func archiveURL(typ, summaryURL, ref string) (url string, isTarGz bool) {
	switch typ {
	case "github":
		return summaryURL + "/archive/refs/" + ref + ".zip", false
	case "forgejo", "gitea":
		return summaryURL + "/archive/" + ref + ".zip", false
	case "gitsrht":
		return summaryURL + "/archive/" + ref + ".tar.gz", true
	case "hgsrht":
		return summaryURL + "/archive/" + ref + ".tar.gz", true
	}
	return "", false
}

func (m module) downloadZIP(client *http.Client, tmpDir string) (*zip.ReadCloser, error) {
	zipPath := filepath.Join(tmpDir, m.name+".zip")
	copier := ioCopy
	if m.ArchiveIsTarGz {
		copier = tarGzipToZip
	}
	if err := downloadFile(client, m.ArchiveURL, zipPath, copier); err != nil {
		return nil, err
	}
	return zip.OpenReader(zipPath)
}

func downloadFile(client *http.Client, url, name string, copier func(io.Writer, io.Reader) error) error {
	resp, err := client.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("%s: unexpected http status code: %d", url, resp.StatusCode)
	}
	if typ := resp.Header.Get("Content-Type"); !strings.HasPrefix(typ, "application/") {
		log.Printf("suspicious Content-Type=%q url=%s\n", typ, url)
	}

	if err := os.MkdirAll(filepath.Dir(name), 0o777); err != nil {
		return err
	}
	dst, err := os.Create(name)
	if err != nil {
		return err
	}
	defer dst.Close()
	return copier(dst, resp.Body)
}

func ioCopy(dst io.Writer, src io.Reader) error {
	_, err := io.Copy(dst, src)
	return err
}

func tarGzipToZip(dst io.Writer, src io.Reader) error {
	gr, err := gzip.NewReader(src)
	if err != nil {
		return err
	}
	defer gr.Close()

	zw := zip.NewWriter(dst)

	tr := tar.NewReader(gr)
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break // End of archive
		}
		if err != nil {
			return err
		}

		if hdr.Typeflag != tar.TypeReg {
			continue
		}
		w, err := zw.Create(hdr.Name)
		if err != nil {
			return err
		}
		if _, err := io.Copy(w, tr); err != nil {
			return err
		}
	}
	return zw.Close()
}
