// SPDX-FileCopyrightText: 2022 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: BSD-3-Clause

package main

/*
https://pkg.go.dev/golang.org/x/tools/go/vcs package is far behind
https://pkg.go.dev/cmd/go/internal/vcs

It is lacking the Cmd.Status field, which would be needed to deduce the current module version.

Maybe ping https://github.com/dmitshur who commented on the issue:
https://github.com/golang/go/issues/11490#issuecomment-346451160
*/
/*
TODO: support "Deprecated" comments
*/
