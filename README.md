<!--
SPDX-FileCopyrightText: 2024 Olivier Charvin <git@olivier.pfad.fr>

SPDX-License-Identifier: CC0-1.0
-->

![Vanitydoc Gopher - based on Gopherize.me output - Artwork by Ashley McNamara - inspired by Renee French - Web app by Mat Ryer](./template/vanitydoc-based-on-gopherize.me.png "Vanitydoc Gopher - based on Gopherize.me generation - Artwork by Ashley McNamara - inspired by Renee French - Web app by Mat Ryer")

# vanitydoc [![Vanitydoc Reference](https://code.pfad.fr/vanitydoc.svg)](https://code.pfad.fr/vanitydoc/) [![Pipeline Status](https://ci.codeberg.org/api/badges/13117/status.svg)](https://ci.codeberg.org/repos/13117) [![Go Report Card](https://goreportcard.com/badge/code.pfad.fr/vanitydoc)](https://goreportcard.com/report/code.pfad.fr/vanitydoc)

Generate Go documentation as a static site (HTML and Gemtext). When served under the import path URL, it makes the package importable by the Go tools ("vanity import path").

For Go reference and a demo, visit https://code.pfad.fr/vanitydoc. To view an example with all the godoc features, visit https://code.pfad.fr/fluhus/godoc-tricks/.

## Configuration

Create a JSON file and give its path as argument to the `vanitydoc` executable. Example configuration file:

```json
{
  "defaults": { // allows to specify defaults for all "modules" below
    // forgejo, gitsrht, hgsrht and github are supported
    "type": "forgejo",

    // the module name will be appended to construct the forge url
    "url_prefix": "https://codeberg.org/pfad.fr/",
    "default_branch": "main"
  },

  "modules": {
    "vanitydoc": {}, // use all defaults for the module hosted at "{url_prefix}/vanitydoc"
    "risefront": {
      "ref": "v1.0.0" // specify the ref to retrieve for this module
    },
    "fluhus/godoc-tricks": {
      "type": "github",
      "url": "https://github.com/fluhus/godoc-tricks",
      "default_branch": "master",
      "ref": "master",

      // github tweak to download the archive of a branch
      // (use "tags/" when "ref" is a tag)
      "archive_ref_prefix": "heads/"
    },
    "exp": {
      "type": "gitsrht",
      "url": "https://git.sr.ht/~oliverpool/exp"
    }
  },

  "html": {
    "output": "dist/http"

    // uncomment to add your templates to customize the HTML
    // "template.pattern": "header.gohtml",

    // uncomment to add a file under "info/refs.redirect" containing the clone url
    // "first_clone_url_info_refs_path": "info/refs.redirect"
  },
  "gemtext": {
    // uncomment the following line to generate documentation as gemtext
    // "output": "dist/gmi"
  },

  "max_conns_per_host": 3 // be nice with the forge when downloading module archives
}
```

Overview of the sections of the JSON (reference below):

```json
{
	"defaults": {...}, // allows to specify defaults for all "modules" below
	"modules": {...}, // modules map: "module_name": {...}

	"html": {...}, // HTML output settings
	"gemtext": {...}, // Gemtext output settings

	"max_conns_per_host": 3 // be nice with the forge when downloading module archives
}
```

### `defaults` object

Allows to specify some defaults for the `modules`:

- `type`: type of forge. One of `forgejo`, `gitsrht`, `hgsrht` or `github`. See https://code.pfad.fr/vanitydoc/autodiscovery/#New for an up-to-date list.
- `url_prefix`: URL prefix. When generating the `modules`, the name of the module will be appended to this prefix to construct the full URL.
- `default_branch`: default branch (used as VCS hint)
- `ref`: default reference to generate the documentation for (e.g. `main`, `v1.0.0`)
- `archive_ref_prefix`: prefix to download the given ref (required for github: "tags/" when `ref` is a tag, "heads/" for branches)

### `modules` objects

The keys of this object are the module names (used as folder-path/URI in the output folders), the value is an object describing the module:

- `type`: type of forge. One of `forgejo`, `gitsrht`, `hgsrht` or `github`. See https://code.pfad.fr/vanitydoc/autodiscovery/#New for an up-to-date list.
- `url`: URL of the VCS (if set, the `defaults.url_prefix` is ignored).
- `default_branch`: default branch (used as VCS hint)
- `ref`: default reference to generate the documentation for (e.g. `main`, `v1.0.0`)
- `archive_ref_prefix`: prefix to download the given ref (required for github: "tags/" when `ref` is a tag, "heads/" for branches)

- `archive_url`: url to download the archive (zip or tar.gz) containing the module
- `archive_targz`: set to `true` if the archive is a tar.gz file

### `html` object

- `output` specifies the root of output folder for the `.html` files. If empty, no HTML will be generated. Each module documentation will be generated under `{output}/{module_name}` (`{module_name}` being the keys in the `modules` object above).
- `stylesheet_links` when set, will be used as stylesheet links for all pages. When not set, a default CSS file will be generated and used (see the note below to generate this file).

  > The default CSS file can be written to an arbitrary folder by running `echo '{ "html": { "output": "dist" } }' | vanitydoc -` (replace `dist` with the desired output folder).

- `template.root` when set, indicates the root folder for template customization.
- `template.pattern` when set, indicates a pattern to look for template customization. See the [layout template](./template/templates/layout.gohtml) to check the block names to customize. [Example of custom header for code.pfad.fr](https://git.sr.ht/~oliverpool/code.pfad.fr/tree/main/item/header.gohtml).
- `first_clone_url_info_refs_path` when set, a file with this name will be created for each module. The file will contain the first clone URL of the module. For vanitydoc, this file is used to set-up a redirection to the actual forge, allowing to run `git clone https://code.pfad.fr/vanitydoc`, see my [blog post](https://log.pfad.fr/2023/git-clone-all-the-things/) for details.

### `gemtext` object - [Project Gemini](https://geminiprotocol.net)

- `output` specifies the root of output folder for the `.gmi` files. If empty, no Gemtext will be generated. Each module documentation will be generated under `{output}/{module_name}` (`{module_name}` being the keys in the `modules` object above).

### `max_conns_per_host` integer

Set the [`(http.Transport).MaxConnsPerHost`](https://pkg.go.dev/net/http#Transport) used for downloading the modules as zip archives from the forge.

> `MaxConnsPerHost` optionally limits the total number of
> connections per host, including connections in the dialing,
> active, and idle states. On limit violation, dials will block.
>
> Zero means no limit.
